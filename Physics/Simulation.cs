﻿using BulletSharp;
using OpenTK;

namespace nIceFramework.Physics
{
    public class Simulation
    {
        DbvtBroadphase broadphase;
        DefaultCollisionConfiguration collisionConfig;
        CollisionDispatcher dispatcher;
        SequentialImpulseConstraintSolver solver;
        DiscreteDynamicsWorld world;

        public Simulation()
        {
            broadphase = new DbvtBroadphase();
            collisionConfig = new DefaultCollisionConfiguration();
            dispatcher = new CollisionDispatcher(collisionConfig);
            solver = new SequentialImpulseConstraintSolver();
            world = new DiscreteDynamicsWorld(dispatcher, broadphase, solver, collisionConfig) { Gravity = new Vector3(0, -9.8f, 0) };
            world.PairCache.SetInternalGhostPairCallback(new GhostPairCallback());
        }

        public void AddBody(RigidBody body)
        {
            world.AddRigidBody(body.Body);
        }

        internal void AddGhost(GhostObject ghost)
        {
            world.AddCollisionObject(ghost);
        }

        internal void Update()
        {
            world.StepSimulation(1 / 60f);
        }

        internal void Dispose()
        {
            world.Dispose();
            solver.Dispose();
            dispatcher.Dispose();
            collisionConfig.Dispose();
            broadphase.Dispose();
        }
        
        public OverlappingPairCache PairCache
        {
            get { return world.PairCache; }
        }

    }
}
