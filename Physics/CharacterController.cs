﻿using BulletSharp;
using OpenTK;
using nIceFramework.Math;

namespace nIceFramework.Physics
{
    public class CharacterController
    {
        public Transform Transform { get; private set; }
        public Vector3 Gravity { get; set; } = new Vector3(0, -9/8f, 0);

        PairCachingGhostObject ghost;
        Vector3 flyvelocity;

        public CharacterController(Vector3 startpos)
        {
            Transform = new Transform() { Position = startpos };

            ghost = new PairCachingGhostObject();
            ghost.CollisionShape = new SphereShape(1);
            ghost.CollisionFlags = CollisionFlags.KinematicObject;
            ghost.WorldTransform = Transform.Matrix;
            Game.Simulation.AddGhost(ghost);
            Transform.Ghost = ghost;
        }

        public void Step(Vector2 direction)
        {
            var relf = Vector3.Transform(new Vector3(direction.X, 0, direction.Y), Transform.Rotation);
            relf.Y = 0;
            relf = relf.Normalized() * direction.Length;
            Transform.Position += relf;
        }

        public void Update()
        {
            bool falling = true;

            var pairs = ghost.OverlappingPairCache.OverlappingPairArray;
            for (int i = 0; i < pairs.Count; i++)
            {
                AlignedManifoldArray manifolds = new AlignedManifoldArray();
                Game.Simulation
                    .PairCache
                    .FindPair(pairs[i].Proxy0, pairs[i].Proxy1)
                    .Algorithm?
                    .GetAllContactManifolds(manifolds);

                for (int j = 0; j < manifolds.Count; j++)
                {
                    PersistentManifold manifold = manifolds[j];
                    bool isfirst = manifold.Body0 == ghost;
                    if ((isfirst ? manifold.Body1 : manifold.Body0).IsStaticOrKinematicObject)
                    {
                        for (int p = 0; p < manifold.NumContacts; p++)
                        {
                            var pt = manifold.GetContactPoint(p);
                            if (pt.Distance < 0)
                                Transform.Position += pt.NormalWorldOnB * pt.Distance * (isfirst ? -1 : 1);

                            falling = false;
                        }
                    }
                }
            }

            if (falling)
            {
                flyvelocity += Gravity / 60f;
                Transform.Position += flyvelocity;
            }
            else
            {
                flyvelocity.Y = 0;
            }
        }
    }
}
