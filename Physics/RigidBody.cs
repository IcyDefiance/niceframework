﻿using BulletSharp;
using nIceFramework.Graphics;
using nIceFramework.Math;
using OpenTK;
using System.Collections.Generic;

namespace nIceFramework.Physics
{
    public class RigidBody
    {
        public BulletSharp.RigidBody Body { get; set; }

        TriangleMesh mesh;
        CollisionShape shape;
        DefaultMotionState state;
        RigidBodyConstructionInfo consInfo;

        public RigidBody(Transform transform, float mass, bool kinematic = false)
        {
            shape = new SphereShape(1);
            state = new DefaultMotionState(transform.Matrix);
            consInfo = new RigidBodyConstructionInfo(mass, state, shape) { Restitution = .5f, Friction = .3f, RollingFriction = .15f };
            Body = new BulletSharp.RigidBody(consInfo);
            if (kinematic)
            {
                Body.CollisionFlags = CollisionFlags.KinematicObject;
                Body.SetSleepingThresholds(0, 0);
            }

            transform.Ghost = Body;
        }

        internal RigidBody(List<Vertex> vertices, List<uint> indices, Transform transform, float mass)
        {
            mesh = new TriangleMesh();
            for (int i = 0; i < indices.Count; i += 3)
            {
                mesh.AddTriangle(
                    vertices[(int)indices[i + 0]].Position,
                    vertices[(int)indices[i + 1]].Position,
                    vertices[(int)indices[i + 2]].Position
                );
            }

            if (mass > 0)
                shape = new SphereShape(1);
            else
                shape = new BvhTriangleMeshShape(mesh, true);
            state = new DefaultMotionState(transform.Matrix);
            consInfo = new RigidBodyConstructionInfo(mass, state, shape, shape.CalculateLocalInertia(mass)) { Restitution = .5f, Friction = .3f, RollingFriction = .15f };
            Body = new BulletSharp.RigidBody(consInfo);

            transform.Ghost = Body;
        }

        ~RigidBody()
        {
            Body.Dispose();
            consInfo.Dispose();
            state.Dispose();
            shape.Dispose();
            mesh?.Dispose();
        }
    }
}
