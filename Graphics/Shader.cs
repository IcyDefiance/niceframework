﻿using OpenTK;
using OpenTK.Graphics.OpenGL4;
using System;
using System.Collections.Generic;

namespace nIceFramework.Graphics
{
    internal class Shader
    {
        public string VertexSource { get; set; }
        public string FragmentSource { get; set; }
        public string GeometrySource { get; set; }

        private int program, vertexShader, fragmentShader, geometryShader;
        private Dictionary<string, int> uniformDictionary;
        private bool disposed = false;

        public Shader()
        {
            program = vertexShader = fragmentShader = 0;
        }

        public void Compile()
        {
            uniformDictionary = new Dictionary<string, int>();

            vertexShader = GL.CreateShader(ShaderType.VertexShader);
            GL.ShaderSource(vertexShader, VertexSource);
            GL.CompileShader(vertexShader);

            fragmentShader = GL.CreateShader(ShaderType.FragmentShader);
            GL.ShaderSource(fragmentShader, FragmentSource);
            GL.CompileShader(fragmentShader);

            if (GeometrySource != null)
            {
                geometryShader = GL.CreateShader(ShaderType.GeometryShader);
                GL.ShaderSource(geometryShader, GeometrySource);
                GL.CompileShader(geometryShader);
            }

            program = GL.CreateProgram();
            GL.AttachShader(program, vertexShader);
            GL.AttachShader(program, fragmentShader);
            if (GeometrySource != null)
                GL.AttachShader(program, geometryShader);
            GL.LinkProgram(program);

            int shaderParams = -1;
            GL.GetShader(vertexShader, ShaderParameter.CompileStatus, out shaderParams);
            if (shaderParams != 1)
                throw new InvalidProgramException(GL.GetShaderInfoLog(vertexShader));

            shaderParams = -1;
            GL.GetShader(fragmentShader, ShaderParameter.CompileStatus, out shaderParams);
            if (shaderParams != 1)
                throw new InvalidProgramException(GL.GetShaderInfoLog(fragmentShader));

            if (GeometrySource != null)
            {
                shaderParams = -1;
                GL.GetShader(geometryShader, ShaderParameter.CompileStatus, out shaderParams);
                if (shaderParams != 1)
                    throw new InvalidProgramException(GL.GetShaderInfoLog(geometryShader));
            }

            shaderParams = -1;
            GL.GetProgram(program, GetProgramParameterName.LinkStatus, out shaderParams);
            if (shaderParams != 1)
                throw new InvalidProgramException("Could not link shader program " + program);
        }

        public void Use()
        {
            GL.UseProgram(program);
        }

        public void SetUniform(string name, int data)
        {
            GL.Uniform1(FindUniformLocation(name), data);
        }

        public void SetUniform(string name, float data)
        {
            GL.Uniform1(FindUniformLocation(name), data);
        }

        public void SetUniform(string name, Vector2 data)
        {
            GL.Uniform2(FindUniformLocation(name), data);
        }

        public void SetUniform(string name, Vector2[] data)
        {
            float[] dataFloats = new float[data.Length * 2];
            for (int i = 0; i < data.Length; i++)
            {
                dataFloats[i * 2] = data[i].X;
                dataFloats[i * 2 + 1] = data[i].Y;
            }
            GL.Uniform2(FindUniformLocation(name), data.Length, dataFloats);
        }

        public void SetUniform(string name, Vector3 data)
        {
            GL.Uniform3(FindUniformLocation(name), data);
        }

        public void SetUniform(string name, Vector4 data)
        {
            GL.Uniform4(FindUniformLocation(name), data);
        }

        public void SetUniform(string name, Matrix4 data)
        {
            OpenTK.Matrix4 tkData = data;
            GL.UniformMatrix4(FindUniformLocation(name), false, ref tkData);
        }

        private int FindUniformLocation(string name)
        {
            int location;
            if (!uniformDictionary.TryGetValue(name, out location))
            {
                location = GL.GetUniformLocation(program, name);

                if (location < 0)
                    throw new KeyNotFoundException("Uniform doesn't exist.");

                uniformDictionary[name] = location;
            }

            return location;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    GL.DeleteShader(vertexShader);
                    GL.DeleteShader(fragmentShader);
                    GL.DeleteShader(geometryShader);
                    GL.DeleteProgram(program);
                }

                disposed = true;
            }
        }

        ~Shader()
        {
            Dispose(false);
        }
    }
}