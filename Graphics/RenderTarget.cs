﻿using OpenTK.Graphics.OpenGL4;

namespace nIceFramework.Graphics
{
    internal class RenderTarget : FrameBuffer
    {
        public Texture Texture { get; set; }
        int rb;

        public RenderTarget(int width, int height)
        {
            Texture = new Texture(width, height);
            Width = width;
            Height = height;

            rb = GL.GenRenderbuffer();
            GL.BindRenderbuffer(RenderbufferTarget.Renderbuffer, rb);
            GL.RenderbufferStorage(RenderbufferTarget.Renderbuffer, RenderbufferStorage.DepthComponent32f, width, height);

            FbID = GL.GenFramebuffer();
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, FbID);
            GL.FramebufferTexture2D(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment0, TextureTarget.Texture2D, Texture.TexID, 0);
            GL.FramebufferRenderbuffer(FramebufferTarget.Framebuffer, FramebufferAttachment.DepthAttachment, RenderbufferTarget.Renderbuffer, rb);
            GL.DrawBuffer(DrawBufferMode.ColorAttachment0);

            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
        }
    }
}