﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace nIceFramework.Graphics
{
    [StructLayout(LayoutKind.Sequential)]
    internal struct Vertex
    {
        public Vector3 Position, Normal;
        public Vector2 TexCoord;
        public Vector3 Tangent, Bitangent;
    }

    internal struct MaterialGroup
    {
        public string Texture;
        public int Offset, Count;
    }

    internal class ObjFile
    {
        public List<Vertex> Vertices = new List<Vertex>();
        public List<uint> Indices = new List<uint>();
        public List<MaterialGroup> Groups = new List<MaterialGroup>();
        public string RelativePath = "";

        private Dictionary<string, string> Materials = new Dictionary<string, string>();
        private List<Vector3> Positions = new List<Vector3>();
        private List<Vector3> Normals = new List<Vector3>();
        private List<Vector2> TexCoords = new List<Vector2>();
        private Dictionary<Vertex, uint> IndexMap = new Dictionary<Vertex, uint>();
        private List<Vertex> PolygonSoup = new List<Vertex>();
        private string NewMaterialName = "", MaterialCursor = "";
        private Vertex? A = null, B = null;
        private MaterialGroup CurrentGroup;

        private uint GetIndex(Vertex v)
        {
            uint idx;
            if (IndexMap.TryGetValue(v, out idx))
            {
                return idx;
            }
            idx = (uint)Vertices.Count;
            Vertices.Add(v);
            IndexMap[v] = idx;
            return idx;
        }

        private void PostProcess()
        {
            if (CurrentGroup.Count > 0) Groups.Add(CurrentGroup);
            foreach (Vertex v in PolygonSoup)
            {
                Indices.Add(GetIndex(v));
            }
        }

        private void ParseFacetPoint(int vi, int vti = -1, int vni = -1)
        {
            Vertex C;
            C.Position = Positions[vi - 1];
            C.TexCoord = (vti < 0) ? Vector2.Zero : TexCoords[vti - 1];
            C.Normal = (vni < 0) ? Vector3.Zero : Normals[vni - 1];
            C.Tangent = Vector3.Zero;
            C.Bitangent = Vector3.Zero;
            if (A == null)
            {
                A = C;
            }
            else if (B == null)
            {
                B = C;
            }
            else
            {
                PolygonSoup.Add(A.Value);
                PolygonSoup.Add(B.Value);
                PolygonSoup.Add(C);
                B = C;

                if (MaterialCursor != CurrentGroup.Texture)
                {
                    if (CurrentGroup.Count > 0) Groups.Add(CurrentGroup);
                    CurrentGroup.Texture = MaterialCursor;
                    CurrentGroup.Offset += CurrentGroup.Count;
                    CurrentGroup.Count = 3;
                }
                else
                {
                    CurrentGroup.Count += 3;
                }
            }
        }

        private bool ParseFacet(string line)
        {
            int vi, vti, vni;
            string buf = "";
            A = B = null;
            vi = vti = vni = -1;
            for (int i = 0; i < line.Length; i++)
            {
                char c = line[i];
                if (c == '/')
                {
                    if (buf.Length == 0)
                    {
                        if (vi == -1)
                        {
                            vi = -2;
                        }
                        else if (vti == -1)
                        {
                            vti = -2;
                        }
                        else
                        {
                            throw new InvalidDataException("Malformed Facet: " + line);
                        }
                    }
                    else
                    {
                        int x = int.Parse(buf);
                        if (vi == -1)
                        {
                            if (x < 0)
                            {
                                x += 1 + Positions.Count;
                            }
                            vi = x;
                        }
                        else if (vti == -1)
                        {
                            if (x < 0)
                            {
                                x += 1 + TexCoords.Count;
                            }
                            vti = x;
                        }
                        else
                        {
                            throw new InvalidDataException("Malformed Facet: " + line);
                        }
                        buf = "";
                    }
                }
                else if (Char.IsWhiteSpace(c))
                {
                    if (buf.Length > 0)
                    {
                        int x = int.Parse(buf);
                        if (vi == -1)
                        {
                            if (x < 0)
                            {
                                x += 1 + Positions.Count;
                            }
                            vi = x;
                        }
                        else if (vti == -1)
                        {
                            if (x < 0)
                            {
                                x += 1 + TexCoords.Count;
                            }
                            vti = x;
                        }
                        else if (vni == -1)
                        {
                            if (x < 0)
                            {
                                x += 1 + Normals.Count;
                            }
                            vni = x;
                        }
                        else
                        {
                            throw new InvalidDataException("Malformed Facet: " + line);
                        }
                        buf = "";
                        ParseFacetPoint(vi, vti, vni);
                        vi = vti = vni = -1;
                    }
                }
                else
                {
                    buf += c;
                }
            }
            if (buf.Length > 0)
            {
                int x = int.Parse(buf);
                if (vi == -1)
                {
                    if (x < 0)
                    {
                        x += 1 + Positions.Count;
                    }
                    vi = x;
                }
                else if (vti == -1)
                {
                    if (x < 0)
                    {
                        x += 1 + TexCoords.Count;
                    }
                    vti = x;
                }
                else if (vni == -1)
                {
                    if (x < 0)
                    {
                        x += 1 + Normals.Count;
                    }
                    vni = x;
                }
                else
                {
                    throw new InvalidDataException("Malformed Facet: " + line);
                }
                ParseFacetPoint(vi, vti, vni);
            }
            return true;
        }

        private bool ParseVertexPosition(string line)
        {
            int box = 0;
            float[] val = new float[3];
            string pad = "";
            for (int i = 0; i < line.Length; i++)
            {
                char c = line[i];
                if (Char.IsWhiteSpace(c) && pad.Length > 0)
                {
                    val[box++] = float.Parse(pad);
                    if (box >= val.GetLength(0))
                    {
                        break;
                    }
                    pad = "";
                    continue;
                }
                pad += c;
            }
            if (box != val.GetLength(0) - 1)
            {
                throw new InvalidDataException("Malformed Vertex Position: " + line);
            }
            val[box++] = float.Parse(pad);
            Positions.Add(new Vector3(val[0], val[1], val[2]));
            return true;
        }

        private bool ParseVertexTexCoord(string line)
        {
            int box = 0;
            float[] val = new float[2];
            string pad = "";
            for (int i = 0; i < line.Length; i++)
            {
                char c = line[i];
                if (Char.IsWhiteSpace(c) && pad.Length > 0)
                {
                    val[box++] = float.Parse(pad);
                    if (box >= val.GetLength(0))
                    {
                        break;
                    }
                    pad = "";
                    continue;
                }
                pad += c;
            }
            if (box != val.GetLength(0) - 1)
            {
                throw new InvalidDataException("Malformed Vertex Texture Coordinate: " + line);
            }
            val[box++] = float.Parse(pad);
            TexCoords.Add(new Vector2(val[0], val[1]));
            return true;
        }

        private bool ParseVertexNormal(string line)
        {
            int box = 0;
            float[] val = new float[3];
            string pad = "";
            for (int i = 0; i < line.Length; i++)
            {
                char c = line[i];
                if (Char.IsWhiteSpace(c) && pad.Length > 0)
                {
                    val[box++] = float.Parse(pad);
                    if (box >= val.GetLength(0))
                    {
                        break;
                    }
                    pad = "";
                    continue;
                }
                pad += c;
            }
            if (box != val.GetLength(0) - 1)
            {
                throw new InvalidDataException("Malformed Vertex Normal: " + line);
            }
            val[box++] = float.Parse(pad);
            Normals.Add(new Vector3(val[0], val[1], val[2]));
            return true;
        }

        private bool ParseLineOBJ(string line)
        {
            int i = 0;
            string cmd = "";
            line = line.Trim();
            for (; i < line.Length; i++)
            {
                char c = line[i];
                if (Char.IsWhiteSpace(c))
                    break;
                cmd += c;
            }
            switch (cmd.ToLower())
            {
                case "v":
                    ParseVertexPosition(line.Substring(i).Trim());
                    break;
                case "vt":
                    ParseVertexTexCoord(line.Substring(i).Trim());
                    break;
                case "vn":
                    ParseVertexNormal(line.Substring(i).Trim());
                    break;
                case "f":
                    ParseFacet(line.Substring(i).Trim());
                    break;
                case "usemtl":
                    string mtlName = line.Substring(i).Trim();
                    if (!Materials.TryGetValue(mtlName, out MaterialCursor))
                    {
                        Console.WriteLine("Undefined Material Reference: {0}", mtlName);
                        //throw new InvalidDataException("Undefined Material Reference: " + mtlName);
                    }
                    break;
                case "mtllib":
                    string mtlFilename = line.Substring(i).Trim();
                    StreamReader sr = null;
                    try
                    {
                        sr = new StreamReader(RelativePath + "/" + mtlFilename);
                    }
                    catch (FileNotFoundException)
                    {
                        try
                        {
                            sr = new StreamReader(mtlFilename);
                        }
                        catch (FileNotFoundException)
                        {
                            Console.WriteLine("Missing Material Library: {0}", mtlFilename);
                            //throw new InvalidDataException("Missing Material Library: " + mtlFilename);
                        }
                    }
                    if (sr != null) LoadMTL(sr);
                    break;
            }
            return true;
        }

        private bool ParseLineMTL(string line)
        {
            int i = 0;
            string cmd = "";
            line = line.Trim();
            for (; i < line.Length; i++)
            {
                char c = line[i];
                if (Char.IsWhiteSpace(c))
                    break;
                cmd += c;
            }
            switch (cmd.ToLower())
            {
                case "newmtl":
                    NewMaterialName = line.Substring(i).Trim();
                    break;
                case "map_kd":
                    Materials[NewMaterialName] = line.Substring(i).Trim();
                    break;
            }
            return true;
        }

        private bool LoadMTL(StreamReader s)
        {
            while (s.Peek() >= 0)
            {
                string line = s.ReadLine().Trim();
                if (line.Length > 0 && line[0] != '#')
                {
                    ParseLineMTL(line);
                }
            }
            return true;
        }

        public ObjFile(string path)
        {
            FileInfo info = new FileInfo(path);
            RelativePath = info.Directory.FullName;
            StreamReader stream = new StreamReader(path);
            while (stream.Peek() >= 0)
            {
                string line = stream.ReadLine().Trim();
                if (line.Length > 0 && line[0] != '#')
                {
                    ParseLineOBJ(line);
                }
            }
            PostProcess();
        }
    }
}
