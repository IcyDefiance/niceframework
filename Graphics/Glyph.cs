﻿namespace nIceFramework.Graphics
{
    class Glyph
    {
        public Texture Texture { get; set; }
        public float AdvanceWidth { get; set; }
        public float LeftSideBearing { get; set; }
        public float RightSideBearing { get; set; }

        public Glyph(Texture texture, float advanceWidth, float leftSideBearing, float rightSideBearing)
        {
            Texture = texture;
            AdvanceWidth = advanceWidth;
            LeftSideBearing = leftSideBearing;
            RightSideBearing = rightSideBearing;
        }
    }
}
