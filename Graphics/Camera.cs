﻿using nIceFramework.Math;
using OpenTK;
namespace nIceFramework.Graphics
{
    public class Camera
    {
        public Transform Transform { get; private set; }

        Matrix4 projection;
        int minRange;
        int maxRange;
        float fieldOfView;
        Rectangle outputZone;
        bool updateProj = true;

        public Camera(int minRange, int maxRage, float fieldOfView, Rectangle outputZone)
        {
            MinRange = minRange;
            MaxRange = maxRage;
            FieldOfView = fieldOfView;
            OutputZone = outputZone;
            Transform = new Transform();
        }

        public Camera(Vector3 position, int minRange, int maxRage, float fieldOfView, Rectangle outputZone)
            : this(minRange, maxRage, fieldOfView, outputZone)
        {
            Transform.Position = position;
        }

        public int MinRange
        {
            get { return minRange; }
            set { minRange = value; updateProj = true; }
        }

        public int MaxRange
        {
            get { return maxRange; }
            set { maxRange = value; updateProj = true; }
        }

        public float FieldOfView
        {
            get { return fieldOfView; }
            set { fieldOfView = value; updateProj = true; }
        }

        public Rectangle OutputZone
        {
            get { return outputZone; }
            set { outputZone = value; updateProj = true; }
        }

        public Matrix4 View
        {
            get
            {
                var forward = Vector3.Transform(-Vector3.UnitZ, Transform.Matrix);
                var up = Vector3.Transform(Vector3.UnitY, Transform.Matrix) - Transform.AbsolutePosition;
                return Matrix4.LookAt(Transform.AbsolutePosition, forward, up);
            }
        }

        public Matrix4 Projection
        {
            get
            {
                if (updateProj)
                {
                    projection = Matrix4.CreatePerspectiveFieldOfView(FieldOfView, (float)OutputZone.Width / OutputZone.Height, MinRange, MaxRange);
                    updateProj = false;
                }

                return projection;
            }
        }
    }
}
