﻿using OpenTK;
using OpenTK.Graphics.OpenGL4;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace nIceFramework.Graphics
{
    public class Mesh
    {
        internal List<Vertex> Vertices { get; set; }
        internal List<uint> Indices { get; set; }
        internal int VAO { get; set; }
        internal int ElementCount { get; set; }

        internal List<MaterialGroup> MatGroups { get; private set; }

        int vbo, ebo;

        public static Mesh FromFile(string path)
        {
            var mesh = new Mesh();

            ObjFile obj = new ObjFile(path);
            Vector3[] tangents = new Vector3[obj.Indices.Count];
            Vector3[] bitangents = new Vector3[obj.Indices.Count];
            for (int i = 0; i < obj.Indices.Count; i += 3)
            {
                
                Vector3 v0 = obj.Vertices[(int)obj.Indices[i + 0]].Position;
                Vector3 v1 = obj.Vertices[(int)obj.Indices[i + 1]].Position;
                Vector3 v2 = obj.Vertices[(int)obj.Indices[i + 2]].Position;
                Vector2 uv0 = obj.Vertices[(int)obj.Indices[i + 0]].TexCoord;
                Vector2 uv1 = obj.Vertices[(int)obj.Indices[i + 1]].TexCoord;
                Vector2 uv2 = obj.Vertices[(int)obj.Indices[i + 2]].TexCoord;

                Vector3 deltaPos1 = v1 - v0;
                Vector3 deltaPos2 = v2 - v0;
                Vector2 deltaUV1 = uv1 - uv0;
                Vector2 deltaUV2 = uv2 - uv0;

                float r = 1.0f / (deltaUV1.X * deltaUV2.Y - deltaUV1.Y * deltaUV2.X);
                Vector3 tangent = ((deltaPos1 * deltaUV2.Y - deltaPos2 * deltaUV1.Y) * r).Normalized();
                Vector3 bitangent = ((deltaPos2 * deltaUV1.X - deltaPos1 * deltaUV2.X) * r).Normalized();
                tangents[i] = tangents[i + 1] = tangents[i + 2] = tangent;
                bitangents[i] = bitangents[i + 1] = bitangents[i + 2] = bitangent;
            }

            int vertexStride = Marshal.SizeOf(obj.Vertices[0]);
            mesh.MatGroups = obj.Groups;
            mesh.ElementCount = obj.Indices.Count;

            mesh.VAO = GL.GenVertexArray();
            mesh.vbo = GL.GenBuffer();
            mesh.ebo = GL.GenBuffer();

            GL.BindVertexArray(mesh.VAO);

            GL.BindBuffer(BufferTarget.ArrayBuffer, mesh.vbo);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(obj.Vertices.Count * vertexStride), obj.Vertices.ToArray(), BufferUsageHint.StaticDraw);

            GL.BindBuffer(BufferTarget.ElementArrayBuffer, mesh.ebo);
            GL.BufferData(BufferTarget.ElementArrayBuffer, (System.IntPtr)(obj.Indices.Count * sizeof(int)), obj.Indices.ToArray(), BufferUsageHint.StaticDraw);

            GL.EnableVertexAttribArray(0);
            GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, vertexStride, 0);
            GL.EnableVertexAttribArray(1);
            GL.VertexAttribPointer(1, 3, VertexAttribPointerType.Float, false, vertexStride, Vector3.SizeInBytes);
            GL.EnableVertexAttribArray(2);
            GL.VertexAttribPointer(2, 2, VertexAttribPointerType.Float, false, vertexStride, Vector3.SizeInBytes * 2);
            GL.EnableVertexAttribArray(3);
            GL.VertexAttribPointer(3, 3, VertexAttribPointerType.Float, false, vertexStride, Vector3.SizeInBytes * 2 + Vector2.SizeInBytes);
            GL.EnableVertexAttribArray(4);
            GL.VertexAttribPointer(4, 3, VertexAttribPointerType.Float, false, vertexStride, Vector3.SizeInBytes * 3 + Vector2.SizeInBytes);

            mesh.Vertices = obj.Vertices;
            mesh.Indices = obj.Indices;
            return mesh;
        }
    }
}