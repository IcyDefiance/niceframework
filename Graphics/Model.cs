﻿using nIceFramework.Math;
using nIceFramework.Physics;

namespace nIceFramework.Graphics
{
    public class Model
    {
        public Mesh Mesh { get; set; }
        public Texture DiffuseMap { get; set; }
        public Texture NormalMap { get; set; }
        public Texture SpecularMap { get; set; }
        public Transform Transform { get; set; }

        public Model(Mesh mesh, Transform transform, Texture texture, Texture normalMap, Texture specularMap, bool applyPhysics, int mass)
        {
            Mesh = mesh;
            Transform = transform;
            DiffuseMap = texture;
            NormalMap = normalMap;
            SpecularMap = specularMap;

            if (applyPhysics)
            {
                var body = new RigidBody(mesh.Vertices, mesh.Indices, Transform, mass);
                Game.Simulation.AddBody(body);
            }
        }
    }
}
