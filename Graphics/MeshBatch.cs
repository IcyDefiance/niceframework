﻿using nIceFramework.Graphics.Lights;
using OpenTK;
using OpenTK.Graphics.OpenGL4;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace nIceFramework.Graphics
{
    public class MeshBatch : IEnumerable
    {
        List<Model> toDraw = new List<Model>();
        List<PointLight> pointLights = new List<PointLight>();
        List<DirectionalLight> directionalLights = new List<DirectionalLight>();

        static MultiRenderTarget gtarget;
        static Shader gshader, plshader, dlshader, alshader;
        static int vbo, vao;

        static MeshBatch()
        {
            gshader = new Shader() { VertexSource = GetShader("geomV"), FragmentSource = GetShader("geomF") };
            gshader.Compile();
            gshader.Use();
            gshader.SetUniform("normMap", 1);
            gshader.SetUniform("specMap", 2);

            var lightV = GetShader("lightV");
            plshader = new Shader() { VertexSource = lightV, FragmentSource = GetShader("plightF") };
            plshader.Compile();
            plshader.Use();
            plshader.SetUniform("position", 1);
            plshader.SetUniform("normal", 2);
            plshader.SetUniform("specular", 3);

            dlshader = new Shader() { VertexSource = lightV, FragmentSource = GetShader("dlightF") };
            dlshader.Compile();
            dlshader.Use();
            dlshader.SetUniform("position", 1);
            dlshader.SetUniform("normal", 2);
            plshader.SetUniform("specular", 3);

            alshader = new Shader() { VertexSource = lightV, FragmentSource = GetShader("alightF") };
            alshader.Compile();

            gtarget = new MultiRenderTarget(Game.Window.Width, Game.Window.Height);

            vbo = GL.GenBuffer();
            vao = GL.GenVertexArray();
            Vector2[] quadPoints = new Vector2[4];
            quadPoints[0] = new Vector2(-1, 1);
            quadPoints[1] = new Vector2(-1, -1);
            quadPoints[2] = new Vector2(1, 1);
            quadPoints[3] = new Vector2(1, -1);
            GL.BindVertexArray(vao);
            GL.BindBuffer(BufferTarget.ArrayBuffer, vbo);
            GL.BufferData(BufferTarget.ArrayBuffer, (System.IntPtr)(OpenTK.BlittableValueType.StrideOf(quadPoints) * quadPoints.Length), quadPoints, BufferUsageHint.StaticDraw);
            GL.EnableVertexAttribArray(0);
            GL.VertexAttribPointer(0, 2, VertexAttribPointerType.Float, false, OpenTK.BlittableValueType.StrideOf(quadPoints), 0);
        }
        
        public void Draw(Camera camera)
        {
            gshader.Use();
            FrameBuffer prevTarget = Game.CurrentTarget;
            Game.BindTarget(gtarget);
            GL.ClearColor(0, 0, 0, 0);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            foreach (var model in toDraw)
            {
                gshader.SetUniform("view", camera.View);
                gshader.SetUniform("proj", camera.Projection);
                gshader.SetUniform("world", model.Transform.Matrix);
                GL.ActiveTexture(TextureUnit.Texture0);
                GL.BindTexture(TextureTarget.Texture2D, model.DiffuseMap.TexID);
                GL.ActiveTexture(TextureUnit.Texture1);
                GL.BindTexture(TextureTarget.Texture2D, model.NormalMap.TexID);
                GL.ActiveTexture(TextureUnit.Texture2);
                GL.BindTexture(TextureTarget.Texture2D, model.SpecularMap.TexID);

                GL.BindVertexArray(model.Mesh.VAO);
                GL.DrawElements(BeginMode.Triangles, model.Mesh.ElementCount, DrawElementsType.UnsignedInt, 0);
            }
            Game.BindTarget(prevTarget);
            GL.ClearColor(Game.ClearColor);

            GL.Disable(EnableCap.DepthTest);
            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);

            GL.BindVertexArray(vao);
            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindTexture(TextureTarget.Texture2D, gtarget.Diffuse.TexID);
            GL.ActiveTexture(TextureUnit.Texture1);
            GL.BindTexture(TextureTarget.Texture2D, gtarget.Position.TexID);
            GL.ActiveTexture(TextureUnit.Texture2);
            GL.BindTexture(TextureTarget.Texture2D, gtarget.Normal.TexID);
            GL.ActiveTexture(TextureUnit.Texture3);
            GL.BindTexture(TextureTarget.Texture2D, gtarget.Specular.TexID);

            alshader.Use();
            GL.DrawArrays(PrimitiveType.TriangleStrip, 0, 4);

            plshader.Use();
            foreach (var light in pointLights)
            {
                plshader.SetUniform("campos", camera.Transform.AbsolutePosition);
                plshader.SetUniform("lightpos", light.Position);
                plshader.SetUniform("range", light.Range);
                GL.DrawArrays(PrimitiveType.TriangleStrip, 0, 4);
            }

            dlshader.Use();
            foreach (var light in directionalLights)
            {
                dlshader.SetUniform("campos", camera.Transform.AbsolutePosition);
                dlshader.SetUniform("lightdir", light.Direction);
                dlshader.SetUniform("power", light.Power);
                GL.DrawArrays(PrimitiveType.TriangleStrip, 0, 4);
            }

            GL.Disable(EnableCap.Blend);
            GL.Enable(EnableCap.DepthTest);

            //sb.Draw(gtarget.Diffuse, Vector2.Zero, new Vector2(1 / 4f));
            //sb.Draw(gtarget.Position, new Vector2(Game.Window.Width - Game.Window.Width / 4, 0), new Vector2(1 / 4f));
            //sb.Draw(gtarget.Normal, new Vector2(0, Game.Window.Height - Game.Window.Height / 4), new Vector2(1 / 4f));
            //sb.Flush();

            toDraw.Clear();
            pointLights.Clear();
            directionalLights.Clear();
        }

        public void Add(Model model)
        {
            toDraw.Add(model);
        }

        public void Add(PointLight light)
        {
            pointLights.Add(light);
        }

        public void Add(DirectionalLight light)
        {
            directionalLights.Add(light);
        }

        public IEnumerator GetEnumerator()
        {
            throw new NotImplementedException();
        }

        static string GetShader(string name)
        {
            return File.ReadAllText($"Graphics/Shaders/{name}.glsl");
        }
    }
}
