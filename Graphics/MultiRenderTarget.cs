﻿using OpenTK;
using OpenTK.Graphics.OpenGL4;

namespace nIceFramework.Graphics
{
    internal class MultiRenderTarget : FrameBuffer
    {
        public static MultiRenderTarget CurrentTarget { get; set; }
        public static Vector2 CurrentTargetSize { get; private set; }
        public Texture Diffuse { get; set; }
        public Texture Position { get; set; }
        public Texture Normal { get; set; }
        public Texture Specular { get; set; }

        int rb;

        static MultiRenderTarget()
        {
            CurrentTargetSize = new Vector2(Game.Window.Width, Game.Window.Height);
        }

        public MultiRenderTarget(int width, int height)
        {
            Width = width;
            Height = height;
            Diffuse = new Texture(width, height);
            Position = new Texture(width, height);
            Normal = new Texture(width, height);
            Specular = new Texture(width, height);

            rb = GL.GenRenderbuffer();
            GL.BindRenderbuffer(RenderbufferTarget.Renderbuffer, rb);
            GL.RenderbufferStorage(RenderbufferTarget.Renderbuffer, RenderbufferStorage.DepthComponent32f, width, height);

            FbID = GL.GenFramebuffer();
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, FbID);
            GL.FramebufferTexture2D(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment0, TextureTarget.Texture2D, Diffuse.TexID, 0);
            GL.FramebufferTexture2D(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment1, TextureTarget.Texture2D, Position.TexID, 0);
            GL.FramebufferTexture2D(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment2, TextureTarget.Texture2D, Normal.TexID, 0);
            GL.FramebufferTexture2D(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment3, TextureTarget.Texture2D, Specular.TexID, 0);
            GL.FramebufferRenderbuffer(FramebufferTarget.Framebuffer, FramebufferAttachment.DepthAttachment, RenderbufferTarget.Renderbuffer, rb);
            GL.DrawBuffers(4, new[] { DrawBuffersEnum.ColorAttachment0, DrawBuffersEnum.ColorAttachment1, DrawBuffersEnum.ColorAttachment2, DrawBuffersEnum.ColorAttachment3 });
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
        }
    }
}