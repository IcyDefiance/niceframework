﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Globalization;
using System.Windows.Media;

namespace nIceFramework.Graphics
{
    public class SpriteFont
    {
        public int AdvanceHeight { get; private set; }
        internal Glyph this[char i] { get { return glyphs[i]; } }

        Dictionary<char, Glyph> glyphs;

        public SpriteFont(string file, int size, bool italic = false, bool bold = false)
        {
            FontStyle bmpstyle = FontStyle.Regular;
            StyleSimulations gtfstyle = StyleSimulations.None;
            if (italic)
            {
                bmpstyle |= FontStyle.Italic;
                gtfstyle = StyleSimulations.ItalicSimulation;
            }
            if (bold)
            {
                bmpstyle |= FontStyle.Bold;
                gtfstyle = StyleSimulations.BoldSimulation;
            }
            if (italic && bold)
            {
                gtfstyle = StyleSimulations.BoldItalicSimulation;
            }

            glyphs = new Dictionary<char, Glyph>();

            GlyphTypeface gtf = new GlyphTypeface(new Uri(file), gtfstyle);
            AdvanceHeight = (int)System.Math.Round(gtf.AdvanceHeights[gtf.CharacterToGlyphMap['M']] * size, MidpointRounding.AwayFromZero);

            for (char i = (char)32; i < 127; i++)
            {
                var gmap = gtf.CharacterToGlyphMap[i];
                double advw = gtf.AdvanceWidths[gmap];
                double advh = gtf.AdvanceHeights[gmap];
                double lsb = gtf.LeftSideBearings[gmap];
                double rsb = gtf.RightSideBearings[gmap];
                double bsb = gtf.BottomSideBearings[gmap];
                double bst = gtf.TopSideBearings[gmap];

                int bmpWidth = (int)(System.Math.Round((advw + lsb + System.Math.Abs(rsb)) * size * 15, MidpointRounding.AwayFromZero));
                int bmpHeight = (int)(System.Math.Round((advh + bsb + bst) * size * 15, MidpointRounding.AwayFromZero));

                if (bmpWidth <= 0 || bmpHeight <= 0)
                    glyphs[i] = new Glyph(null, (float)(advw * size), (float)(lsb * size), (float)(rsb * size));
                else
                {
                    using (Bitmap bmp = new Bitmap(bmpWidth, bmpHeight))
                    using (System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(bmp))
                    {
                        g.SmoothingMode = SmoothingMode.AntiAlias;
                        g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                        g.PixelOffsetMode = PixelOffsetMode.HighQuality;
                        g.FillRectangle(System.Drawing.Brushes.Transparent, new Rectangle(0, 0, bmp.Width, bmp.Height));
                        g.DrawString(i.ToString(), new Font(gtf.FamilyNames[CultureInfo.CurrentCulture], size, bmpstyle, GraphicsUnit.Pixel), System.Drawing.Brushes.White, 0, 0);
                        g.Flush();
                        glyphs[i] = new Glyph(new Texture(bmp), (float)(advw * size), (float)(lsb * size), (float)(rsb * size));
                    }
                }
            }
        }
    }
}
