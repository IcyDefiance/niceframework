﻿using OpenTK;
using OpenTK.Graphics.OpenGL4;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;

namespace nIceFramework.Graphics
{
    public class SpriteBatch
    {
        List<TexturePositionMap> toDraw = new List<TexturePositionMap>();
        static Shader shader;
        static int vbo, vao;

        static SpriteBatch()
        {
            if (shader == null)
            {
                shader = new Shader() { VertexSource = File.ReadAllText("Graphics/Shaders/spriteV.glsl"), FragmentSource = File.ReadAllText("Graphics/Shaders/spriteF.glsl") };
                shader.Compile();

                vbo = GL.GenBuffer();
                vao = GL.GenVertexArray();

                GL.BindVertexArray(vao);

                var quadPoints = Enumerable.Range(0, 4).Select(x => new SpriteElement()).ToArray();
                quadPoints[0].Vertex = new Vector2(0, 0);
                quadPoints[0].TexCoord = Vector2.UnitY;
                quadPoints[1].Vertex = new Vector2(0, -2);
                quadPoints[1].TexCoord = Vector2.Zero;
                quadPoints[2].Vertex = new Vector2(2, 0);
                quadPoints[2].TexCoord = Vector2.One;
                quadPoints[3].Vertex = new Vector2(2, -2);
                quadPoints[3].TexCoord = Vector2.UnitX;

                GL.BindBuffer(BufferTarget.ArrayBuffer, vbo);
                GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(BlittableValueType.StrideOf(quadPoints) * quadPoints.Length), quadPoints, BufferUsageHint.StaticDraw);
                GL.EnableVertexAttribArray(0);
                GL.VertexAttribPointer(0, 2, VertexAttribPointerType.Float, false, BlittableValueType.StrideOf(quadPoints), 0);
                GL.EnableVertexAttribArray(1);
                GL.VertexAttribPointer(1, 2, VertexAttribPointerType.Float, false, BlittableValueType.StrideOf(quadPoints), Marshal.SizeOf(typeof(Vector2)));
            }
        }

        public void Draw()
        {
            Vector2 lastPos, lastScale, lastTexSize, lastTarSize;
            Vector4 lastTint;
            lastPos = lastScale = lastTexSize = lastTarSize = Vector2.Zero;
            lastTint = Vector4.One;
            int lastTexID = -1;

            GL.Disable(EnableCap.DepthTest);
            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);

            shader.Use();
            GL.BindVertexArray(vao);
            GL.ActiveTexture(TextureUnit.Texture0);
            shader.SetUniform("pos", lastPos);
            shader.SetUniform("texSize", lastTexSize);
            shader.SetUniform("tarSize", lastTarSize);
            shader.SetUniform("scale", lastScale);
            shader.SetUniform("tint", lastTint);
            foreach (var tMap in toDraw)
            {
                if (tMap.Position != lastPos)
                {
                    lastPos = tMap.Position;
                    shader.SetUniform("pos", lastPos);
                }
                if (tMap.Scale != lastScale)
                {
                    lastScale = tMap.Scale;
                    shader.SetUniform("scale", lastScale);
                }
                if (tMap.Tint != lastTint)
                {
                    lastTint = tMap.Tint;
                    shader.SetUniform("tint", lastTint);
                }
                if (tMap.Texture.Width != lastTexSize.X || tMap.Texture.Height != lastTexSize.Y)
                {
                    lastTexSize = new Vector2(tMap.Texture.Width, tMap.Texture.Height);
                    shader.SetUniform("texSize", lastTexSize);
                }
                if (Game.CurrentTargetSize != lastTarSize)
                {
                    lastTarSize = Game.CurrentTargetSize;
                    shader.SetUniform("tarSize", lastTarSize);
                }
                if (tMap.Texture.TexID != lastTexID)
                {
                    lastTexID = tMap.Texture.TexID;
                    GL.BindTexture(TextureTarget.Texture2D, lastTexID);
                }

                GL.DrawArrays(PrimitiveType.TriangleStrip, 0, 4);
            }

            GL.Disable(EnableCap.Blend);
            GL.Enable(EnableCap.DepthTest);

            toDraw.Clear();
        }

        public void Add(Texture texture, Vector2 position) { Add(texture, position, Vector2.One, Vector4.One); }
        public void Add(Texture texture, Vector2 position, Vector2 scale) { Add(texture, position, scale, Vector4.One); }
        public void Add(Texture texture, Vector2 position, Vector2 scale, Vector4 tint)
        {
            toDraw.Add(new TexturePositionMap()
            {
                Texture = texture,
                Position = position,
                Scale = scale,
                Tint = tint
            });
        }

        public void Add(SpriteFont font, string text, Vector2 position)
        {
            int tWidth = (int)(text + " ").Where(x => x != '\r' && x != '\n').Select(x => font[x].AdvanceWidth).Sum();
            int tHeight = (int)(font.AdvanceHeight * (text.Count(x => x == '\n') + 1.5f));

            var charPos = Vector2.Zero;

            for (int i = 0; i < text.Length; i++)
            {
                if (text[i] == '\n')
                    charPos = new Vector2(0, charPos.Y + font.AdvanceHeight);
                else if (text[i] != '\r')
                {
                    var glyph = font[text[i]];

                    if (i == 0 || text[i - 1] == '\n' || text[i - 1] == '\r')
                    {
                        charPos -= new Vector2(font[text[i]].LeftSideBearing);
                    }

                    if (glyph.Texture != null)
                    {
                        toDraw.Add(new TexturePositionMap()
                        {
                            Texture = glyph.Texture,
                            Position = position + charPos,
                            Scale = Vector2.One,
                            Tint = Vector4.One
                        });
                    }

                    charPos += new Vector2(glyph.AdvanceWidth, 0);
                }
            }
        }

        struct TexturePositionMap
        {
            public Texture Texture;
            public Vector2 Position;
            public Vector2 Scale;
            public Vector4 Tint;
        }

        [StructLayout(LayoutKind.Sequential)]
        struct SpriteElement
        {
            public Vector2 Vertex;
            public Vector2 TexCoord;
        }
    }
}
