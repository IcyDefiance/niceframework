﻿namespace nIceFramework.Graphics
{
    public class FrameBuffer
    {
        public int FbID { get; protected set; }
        public int Width { get; protected set; }
        public int Height { get; protected set; }
    }
}