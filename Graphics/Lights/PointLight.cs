﻿using OpenTK;

namespace nIceFramework.Graphics.Lights
{
    public class PointLight
    {
        public Vector3 Position { get; set; }
        public float Range { get; set; }

        public PointLight(Vector3 position, float range)
        {
            Position = position;
            Range = range;
        }
    }
}