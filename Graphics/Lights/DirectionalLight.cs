﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nIceFramework.Graphics.Lights
{
    public class DirectionalLight
    {
        public Vector3 Direction { get; set; }
        public float Power { get; set; }

        public DirectionalLight(Vector3 direction, float power)
        {
            Direction = direction;
            Power = power;
        }
    }
}
