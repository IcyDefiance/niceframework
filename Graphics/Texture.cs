﻿using OpenTK;
using OpenTK.Graphics.OpenGL4;
using System;
using System.Drawing;
using System.Drawing.Imaging;

namespace nIceFramework.Graphics
{
    public class Texture
    {
        public static readonly Texture WhitePixel = new Texture(1, 1, new[] { Vector4.One });
        public static readonly Texture DefaultNormal = new Texture(1, 1, new[] { new Vector4(.5f, .5f, 1, 1) });
        public static readonly Texture DefaultSpecular = new Texture(1, 1, new[] { new Vector4(1, 1, 1, .1f) });

        public int Width { get; private set; }
        public int Height { get; private set; }
        internal int TexID { get; set; }

        internal Texture(int width, int height)
        {
            Width = width;
            Height = height;

            TexID = GL.GenTexture();
            GL.BindTexture(TextureTarget.Texture2D, TexID);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba32f, width, height, 0, OpenTK.Graphics.OpenGL4.PixelFormat.Bgra, PixelType.Float, IntPtr.Zero);

            GC.AddMemoryPressure(width * height * 4);
        }

        internal Texture(int width, int height, Vector4[] pixels)
            : this(width, height)
        {
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, width, height, 0, OpenTK.Graphics.OpenGL4.PixelFormat.Rgba, PixelType.Float, pixels);
        }

        internal Texture(Bitmap bitmap)
            : this(bitmap.Width, bitmap.Height)
        {
            bitmap.RotateFlip(RotateFlipType.RotateNoneFlipY);
            BitmapData bmpData = bitmap.LockBits(new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, bmpData.Width, bmpData.Height, 0, OpenTK.Graphics.OpenGL4.PixelFormat.Bgra, PixelType.UnsignedByte, bmpData.Scan0);
            bitmap.UnlockBits(bmpData);
            bitmap.RotateFlip(RotateFlipType.RotateNoneFlipY);
        }

        public static Texture FromFile(string filename)
        {
            using (var bmp = new Bitmap(filename))
                return new Texture(bmp);
        }

        ~Texture()
        {
            if (!Game.IsExiting)
                GL.DeleteTexture(TexID);
        }
    }
}