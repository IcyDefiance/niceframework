﻿#version 420

in vec2 vpos;

out vec2 uv;

void main() {
    uv = (vpos + 1) / 2;
    gl_Position = vec4(vpos, 0, 1);
}