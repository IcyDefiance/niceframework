﻿#version 420

uniform sampler2D diffuse;
uniform sampler2D position;
uniform sampler2D normal;
uniform sampler2D specular;
uniform vec3 lightdir;
uniform vec3 campos;
uniform float power;

in vec2 uv;

out vec4 color;

void main() {
    vec4 fdiffuse = texture(diffuse, uv);
    vec3 fpos = texture(position, uv).xyz;
    vec3 fnormal = texture(normal, uv).xyz;

    vec3 vtolDir = normalize(-lightdir);
    vec3 vtolRef = normalize(reflect(-vtolDir, fnormal));

    vec3 vtocDir = normalize(campos - fpos);

    float NdotL = max(0.0, dot(fnormal, vtolDir));
    float att = clamp(power, 0.0, 1.0);
    vec4 spec = pow(max(0.0, dot(vtocDir, vtolRef)), 50) * texture(specular, uv);
    color = fdiffuse * vec4((NdotL + spec) * att * att);
}