﻿#version 420

layout(location = 0) in vec2 vertPos;
layout(location = 1) in vec2 vertTexcoord;
out vec2 texcoord;

uniform vec2 pos, scale, texSize, tarSize;

void main() {
    texcoord = vertTexcoord;
    gl_Position = vec4(
		(texSize * scale * vertPos + vec2(2, -2) * pos) / tarSize + vec2(-1, 1),
		0.0,
		1.0
	);
}