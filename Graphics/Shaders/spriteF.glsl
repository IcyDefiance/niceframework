﻿#version 420

in vec2 texcoord;

uniform sampler2D tex;
uniform vec4 tint;

out vec4 fragColor;

void main() {
    fragColor = texture(tex, texcoord) * tint;
}