﻿#version 420

uniform sampler2D diffuse;

in vec2 uv;

out vec4 color;

void main() {
    vec4 fdiffuse = texture(diffuse, uv);
    color = fdiffuse * vec4(0.1, 0.1, 0.1, 1);
}