﻿#version 420

layout (location = 0) in vec3 vpos;
layout (location = 1) in vec3 vnormal;
layout (location = 2) in vec2 vtexcoord;
layout (location = 3) in vec3 vtangent;
layout (location = 4) in vec3 vbitangent;

uniform mat4 view, proj, world;

out vec4 wpos;
out vec4 wnormal;
out vec2 uv;
out vec3 tan;
out vec3 bitan;

void main() {
    wpos = world * vec4(vpos, 1);
    wnormal = world * vec4(vnormal, 0);
    uv = vtexcoord;
    tan = vtangent;
    bitan = vbitangent;
    gl_Position = proj * view * wpos;
}