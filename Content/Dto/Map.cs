﻿using nIceFramework.Graphics.Lights;
using System.Collections.Generic;

namespace nIceFramework.Content.Dto
{
    class Map
    {
        public List<File> Files { get; set; }
        public List<Model> Models { get; set; }
        public List<PointLight> PointLights { get; set; }
        public List<DirectionalLight> DirectionalLights { get; set; }
    }
}
