﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nIceFramework.Content.Dto
{
    class File
    {
        public string Name { get; set; }
        public string Filename { get; set; }
    }
}
