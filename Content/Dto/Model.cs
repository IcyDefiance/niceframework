﻿using OpenTK;

namespace nIceFramework.Content.Dto
{
    class Model
    {
        public string MeshName { get; set; }
        public string TexName { get; set; }
        public string NorName { get; set; }
        public string SpecName { get; set; }
        public Vector3 Position { get; set; }
        public int Mass { get; set; }
        public bool Collide { get; set; } = true;
    }
}
