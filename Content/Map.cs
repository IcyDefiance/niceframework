﻿using System.Collections.Generic;
using Newtonsoft.Json;
using nIceFramework.Graphics.Lights;
using System.IO;
using nIceFramework.Graphics;
using nIceFramework.Math;

namespace nIceFramework.Content
{
    public class Map
    {
        public List<Model> Models { get; set; }
        public List<PointLight> PointLights { get; set; }
        public List<DirectionalLight> DirectionalLights { get; set; }

        public Map()
        {
            Models = new List<Model>();
            PointLights = new List<PointLight>();
            DirectionalLights = new List<DirectionalLight>();
        }

        public void AddTo(MeshBatch meshBatch)
        {
            for (int i = 0; i < Models.Count; i++)
                meshBatch.Add(Models[i]);

            for (int i = 0; i < PointLights.Count; i++)
                meshBatch.Add(PointLights[i]);

            for (int i = 0; i < DirectionalLights.Count; i++)
                meshBatch.Add(DirectionalLights[i]);
        }

        public static Map FromFile(string filename)
        {
            var dtos = JsonConvert.DeserializeObject<Dto.Map>(File.ReadAllText(filename));
            var files = new Dictionary<string, object>();
            foreach (var file in dtos.Files)
            {
                if (file.Filename.EndsWith(".obj"))
                    files[file.Name] = Mesh.FromFile(file.Filename);
                else
                    files[file.Name] = Texture.FromFile(file.Filename);
            }

            var map = new Map();
            foreach (var model in dtos.Models)
            {
                var diffMap = model.TexName != null ? (Texture)files[model.TexName] : Texture.WhitePixel;
                var normMap = model.NorName != null ? (Texture)files[model.NorName] : Texture.DefaultNormal;
                var specMap = model.SpecName != null ? (Texture)files[model.SpecName] : Texture.DefaultSpecular;

                map.Models.Add(new Model(
                    (Mesh)files[model.MeshName],
                    new Transform() { Position = model.Position },
                    diffMap,
                    normMap,
                    specMap,
                    model.Collide,
                    model.Mass
                ));
            }

            map.PointLights = dtos.PointLights;
            map.DirectionalLights = dtos.DirectionalLights;

            return map;
        }
    }
}
