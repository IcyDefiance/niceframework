﻿using System;
using OpenTK;
using OpenTK.Graphics.OpenGL4;
using nIceFramework.Graphics;
using OpenTK.Graphics;
using nIceFramework.Physics;

namespace nIceFramework
{
    public static class Game
    {
        public static GameWindow Window { get; private set; }
        public static Vector2 CurrentTargetSize { get; private set; }
        public static FrameBuffer CurrentTarget { get; private set; }
        public static bool IsExiting { get; private set; }
        public static bool ClearEnabled { get; set; }
        public static Simulation Simulation { get; set; }

        static Color4 clearColor;

        static Game()
        {
            Window = new GameWindow(1440, 810, GraphicsMode.Default, "nIceFramework");
            Simulation = new Simulation();
        }

        public static void Start()
        {
            Window.Load += Window_Load;
            Window.UpdateFrame += Window_UpdateFrame;
            Window.RenderFrame += Window_RenderFrame;
            Window.Run(60);
            IsExiting = true;
            Simulation.Dispose();
            Window.Dispose();
        }

        private static void Window_UpdateFrame(object sender, FrameEventArgs e)
        {
            Simulation.Update();
        }

        private static void Window_RenderFrame(object sender, FrameEventArgs e)
        {
            if (ClearEnabled)
                GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
        }

        private static void Window_Load(object sender, EventArgs e)
        {
            GL.Enable(EnableCap.CullFace);
            BindTarget(null);
        }

        public static void BindTarget(FrameBuffer target)
        {
            if (target == null)
            {
                GL.Viewport(Window.ClientRectangle);
                GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
                CurrentTargetSize = new Vector2(Window.Width, Window.Height);
            }
            else
            {
                GL.Viewport(0, 0, target.Width, target.Height);
                GL.BindFramebuffer(FramebufferTarget.Framebuffer, target.FbID);
                CurrentTargetSize = new Vector2(target.Width, target.Height);
            }

            CurrentTarget = target;
        }

        public static Color4 ClearColor
        {
            get { return clearColor; }
            set { GL.ClearColor(value); clearColor = value; }
        }
    }
}
