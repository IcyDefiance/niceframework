﻿using BulletSharp;
using OpenTK;
using System.Collections.Generic;

namespace nIceFramework.Math
{
    public class Transform
    {
        public Transform Parent { get; set; }
        internal CollisionObject Ghost { get; set; }
        
        Vector3 position, scale;
        Quaternion rotation;
        Matrix4 matrix;

        public Transform()
        {
            rotation = Quaternion.Identity;
            matrix = Matrix4.Identity;
            position = Vector3.Zero;
            scale = Vector3.One;
        }

        public void RotateLocalX(float rads)
        {
            Rotation *= Quaternion.FromAxisAngle(Vector3.UnitX, rads);
        }

        public void RotateWorldY(float rads)
        {
            Rotation = Quaternion.FromAxisAngle(Vector3.UnitY, rads) * Rotation;
        }

        public void TranslateLocal(Vector3 vector)
        {
            position = Matrix.ExtractTranslation();
            Position += Vector3.Transform(vector, Rotation);
        }

        void UpdateMatrix()
        {
            var newmat = Matrix4.CreateFromQuaternion(Rotation) * Matrix4.CreateScale(Scale) * Matrix4.CreateTranslation(Position);
            if (Ghost != null)
                Ghost.WorldTransform = newmat;
            else
                matrix = newmat;
        }

        public Vector3 Position
        {
            get
            {
                return position;
            }
            set
            {
                position = value;
                UpdateMatrix();
            }
        }

        public Vector3 AbsolutePosition
        {
            get
            {
                return Vector3.TransformPosition(position, Matrix);
            }
        }

        public Quaternion Rotation
        {
            get
            {
                return rotation;
            }
            set
            {
                rotation = value;
                UpdateMatrix();
            }
        }

        public Vector3 Scale
        {
            get
            {
                return scale;
            }
            set
            {
                scale = value;
                UpdateMatrix();
            }
        }

        public Matrix4 Matrix
        {
            get
            {
                var ret = Ghost?.WorldTransform ?? matrix;
                if (Parent != null)
                    ret *= Parent.Matrix;
                return ret;
            }
        }
    }
}
