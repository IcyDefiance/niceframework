﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nIceFramework.Math
{
    public class Rectangle
    {
        public int X, Y, Width, Height;

        public Rectangle(int x, int y, int width, int height)
        {
            X = x;
            Y = y;
            Width = width;
            Height = height;
        }

        public bool Contains(int x, int y)
        {
            return x >= X &&
                x < X + Width &&
                y >= Y &&
                y < Y + Height;
        }
    }
}
